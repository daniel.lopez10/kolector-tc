import {
  AnyObject,
  FilterExcludingWhere,
  repository,
} from '@loopback/repository';
import {
  get,
  getModelSchemaRef,
  param,
  post,
  requestBody,
  response,
} from '@loopback/rest';
import {Callback} from '../models';
import {CallbackRepository} from '../repositories';

export class CallbackController {
  constructor(
    @repository(CallbackRepository)
    public callbackRepository: CallbackRepository,
  ) {}

  @post('/nuevo')
  @response(200, {
    description:
      'Starts new payment method transaction and return hash provided by payment service',
    content: {'application/json': {schema: getModelSchemaRef(Callback)}},
  })
  async newPayment(
    @requestBody({
      content: {
        'application/json': {
          schema: {
            type: 'object',
            title: 'Callback',
            properties: {
              nombre: {type: 'string'},
              codigo: {type: 'integer'},
            },
          },
        },
      },
    })
    paymenteData: Object,
  ): Promise<Callback> {
    return this.callbackRepository.newPayment(paymenteData);
  }

  @post('/callback/ok')
  @response(200, {
    description: 'Sets callback state to OK',
    content: {'application/json': {schema: getModelSchemaRef(Callback)}},
  })
  async updateOk(
    @requestBody({
      content: {
        'application/json': {
          schema: {
            type: 'object',
            title: 'Callback',
            properties: {
              hash: {type: 'string'},
              idReferenciaOperacionComercio: {type: 'string'},
            },
          },
        },
      },
    })
    callback: Callback,
  ): Promise<AnyObject> {
    return this.callbackRepository.setOkState(callback);
  }

  @post('/callback/cancel')
  @response(200, {
    description: 'Sets callback state to CANCEL',
    content: {'application/json': {schema: getModelSchemaRef(Callback)}},
  })
  async updateCancel(
    @requestBody({
      content: {
        'application/json': {
          schema: {
            type: 'object',
            title: 'Callback',
            properties: {
              hash: {type: 'string'},
              idReferenciaOperacionComercio: {type: 'string'},
            },
          },
        },
      },
    })
    callback: Callback,
  ): Promise<AnyObject> {
    return this.callbackRepository.setCancelState(callback);
  }

  @post('/callback/error')
  @response(200, {
    description: 'Sets callback state to ERROR and save error specs',
    content: {'application/json': {schema: getModelSchemaRef(Callback)}},
  })
  async updateError(
    @requestBody({
      content: {
        'application/json': {
          schema: {
            type: 'object',
            title: 'Callback',
            properties: {
              hash: {type: 'string'},
              idReferenciaOperacionComercio: {type: 'string'},
              error: {
                type: 'object',
                properties: {
                  codigo: {type: 'string'},
                  descripcion: {type: 'string'},
                },
              },
            },
          },
        },
      },
    })
    callback: Callback,
  ): Promise<AnyObject> {
    return this.callbackRepository.setErrorState(callback);
  }

  @get('/callback/{id}')
  @response(200, {
    description:
      'Obtains data from callbacks by id, when callback has no state attribute, it means its pending',
    content: {
      'application/json': {
        schema: getModelSchemaRef(Callback, {includeRelations: true}),
      },
    },
  })
  async findById(
    @param.path.string('id') id: string,
    @param.filter(Callback, {exclude: 'where'})
    filter?: FilterExcludingWhere<Callback>,
  ): Promise<Callback> {
    return this.callbackRepository.findById(id, filter);
  }
}
