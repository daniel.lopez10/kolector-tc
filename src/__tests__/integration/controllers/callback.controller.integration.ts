import {expect} from '@loopback/testlab';
import {CallbackController} from '../../../controllers';
import {Callback} from '../../../models';
import {CallbackRepository} from '../../../repositories';
import {testdb} from '../../fixtures/datasources/testdb.datasource';
import {
  givenCallback,
  givenEmptyDatabase,
} from '../../helpers/database.helpers';

describe('Callback controller (integration)', () => {
  beforeEach(givenEmptyDatabase);

  describe('newPayment()', () => {
    it('Must return a callback object with a hash', async () => {
      const controller = new CallbackController(new CallbackRepository(testdb));
      const callBack = {
        nombre: 'tstHash',
        codigo: '1',
      };

      const call = await controller.newPayment(callBack);

      expect(call).to.containEql({hash: 'tstHash1'});
    });
  });

  describe('findById()', () => {
    it('Must return callback data', async () => {
      const controller = new CallbackController(new CallbackRepository(testdb));
      const existingCB = await givenCallback({hash: 'tstHash12'});

      const callBack = await controller.findById(existingCB.getId());

      expect(callBack).to.containEql({hash: 'tstHash12'});
    });
  });

  describe('updateOk()', () => {
    it('Must return count of updated rows to OK state', async () => {
      const controller = new CallbackController(new CallbackRepository(testdb));
      const existingCB = await givenCallback({hash: 'tstHash123'});
      const callBack = new Callback({
        hash: 'tstHash123',
        idReferenciaOperacionComercio: '123',
      });

      const call = await controller.updateOk(callBack);
      const updatedCB = await controller.findById(existingCB.getId());

      expect(call).to.containEql({count: 1});
      expect(updatedCB).to.containEql({state: 'OK'});
    });
  });

  describe('updateCancel()', () => {
    it('Must return count of updated rows to CANCEL state', async () => {
      const controller = new CallbackController(new CallbackRepository(testdb));
      const existingCB = await givenCallback({hash: 'tstHash1234'});
      const callBack = new Callback({
        hash: 'tstHash1234',
        idReferenciaOperacionComercio: '1234',
      });

      const call = await controller.updateCancel(callBack);
      const updatedCB = await controller.findById(existingCB.getId());

      expect(call).to.containEql({count: 1});
      expect(updatedCB).to.containEql({state: 'CANCEL'});
    });
  });

  describe('updateError()', () => {
    it('Must return count of updated rows to ERROR state and keep error data', async () => {
      const controller = new CallbackController(new CallbackRepository(testdb));
      const existingCB = await givenCallback({hash: 'tstHash12345'});
      const callBack = new Callback({
        hash: 'tstHash12345',
        idReferenciaOperacionComercio: '12345',
        error: {codigo: '123456', descripcion: 'se produjo un error'},
      });

      const call = await controller.updateError(callBack);
      const updatedCB = await controller.findById(existingCB.getId());

      expect(call).to.containEql({count: 1});
      expect(updatedCB).to.containEql({state: 'ERROR'});
      expect(updatedCB).to.containEql({
        error: {codigo: '123456', descripcion: 'se produjo un error'},
      });
    });
  });
});
