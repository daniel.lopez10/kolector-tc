import {
  createStubInstance,
  expect,
  StubbedInstanceWithSinonAccessor,
} from '@loopback/testlab';
import {CallbackController} from '../../controllers';
import {Callback} from '../../models';
import {CallbackRepository} from '../../repositories';

describe('CallbackController (unit)', () => {
  let repository: StubbedInstanceWithSinonAccessor<CallbackRepository>;
  beforeEach(givenStubbedRepository);

  describe('newPayment()', () => {
    it('Should return callbackId and a hash', async () => {
      const controller = new CallbackController(repository);
      const callBack = new Callback({
        idCallBack: '6dashkjhs879',
        hash: 'daniel0',
      });

      repository.stubs.newPayment.resolves(callBack);

      const newCallback = await controller.newPayment({
        nombre: 'daniel',
        codigo: 0,
      });

      expect(newCallback).to.has.property('hash');
    });
  });

  describe('updateOk()', () => {
    it('Should return a count of updated registries', async () => {
      const controller = new CallbackController(repository);
      const callBack = new Callback({
        hash: '6dashkjhs879',
        idReferenciaOperacionComercio: 10,
      });

      repository.stubs.setOkState.resolves({count: 1});

      const okCallback = await controller.updateOk(callBack);

      expect(okCallback).to.eql({count: 1});
    });
  });

  describe('updateCancel()', () => {
    it('Should return a count of updated registries', async () => {
      const controller = new CallbackController(repository);
      const callBack = new Callback({
        hash: '6dashkjhs879',
        idReferenciaOperacionComercio: 10,
      });

      repository.stubs.setCancelState.resolves({count: 1});

      const okCallback = await controller.updateCancel(callBack);

      expect(okCallback).to.eql({count: 1});
    });
  });

  describe('updateError()', () => {
    it('Should return a count of updated registries', async () => {
      const controller = new CallbackController(repository);
      const callBack = new Callback({
        hash: '6dashkjhs879',
        idReferenciaOperacionComercio: 10,
      });

      repository.stubs.setErrorState.resolves({count: 1});

      const okCallback = await controller.updateError(callBack);

      expect(okCallback).to.eql({count: 1});
    });
  });

  function givenStubbedRepository() {
    repository = createStubInstance(CallbackRepository);
  }
});
