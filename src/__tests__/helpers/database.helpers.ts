import {Callback} from '../../models';
import {CallbackRepository} from '../../repositories';
import {testdb} from '../fixtures/datasources/testdb.datasource';

export async function givenEmptyDatabase() {
  await new CallbackRepository(testdb).deleteAll();
}

export function givenCallbackData(data?: Partial<Callback>) {
  return Object.assign(
    {
      hash: 'hash devuelto',
      idOperation: 'numero de operacion',
      error: {},
      state: '',
    },
    data,
  );
}

export async function givenCallback(data?: Partial<Callback>) {
  return new CallbackRepository(testdb).create(givenCallbackData(data));
}
