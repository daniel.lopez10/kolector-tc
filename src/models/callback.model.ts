import {Entity, model, property} from '@loopback/repository';

@model({settings: {strict: false}})
export class Callback extends Entity {
  @property({
    type: 'string',
    id: true,
    generated: true,
  })
  idCallback?: string;

  @property({
    type: 'string',
    required: true,
  })
  hash: string;

  @property({
    type: 'string',
  })
  idOperation?: string;

  @property({
    type: 'object',
  })
  error?: object;

  @property({
    type: 'string',
  })
  state: string;

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<Callback>) {
    super(data);
  }
}

export interface CallbackRelations {
  // describe navigational properties here
}

export type CallbackWithRelations = Callback & CallbackRelations;
