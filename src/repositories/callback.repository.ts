import {inject} from '@loopback/core';
import {AnyObject, DefaultCrudRepository} from '@loopback/repository';
import {LocalMongoDataSource} from '../datasources';
import {Callback, CallbackRelations} from '../models';

export class CallbackRepository extends DefaultCrudRepository<
  Callback,
  typeof Callback.prototype.idCallback,
  CallbackRelations
> {
  constructor(
    @inject('datasources.localMongo') dataSource: LocalMongoDataSource,
  ) {
    super(Callback, dataSource);
  }

  async setOkState(callback: Callback) {
    callback.state = 'OK';
    const updatedCallback = await this.updateAll(callback, {
      hash: callback.hash,
    });
    return updatedCallback;
  }

  async setCancelState(callback: Callback) {
    callback.state = 'CANCEL';
    const updatedCallback = await this.updateAll(callback, {
      hash: callback.hash,
    });
    return updatedCallback;
  }

  async setErrorState(callback: Callback) {
    callback.state = 'ERROR';
    const updatedCallback = await this.updateAll(callback, {
      hash: callback.hash,
    });
    return updatedCallback;
  }

  async newPayment(paymentData: AnyObject) {
    //TODO: Implementar la logica necesaria para llamar al servicio del medio de pago para el que se va a iniciar la transaccion y recibir el hash
    //Dicho hash en esta ocasion a modo de prueba sera la concatenacion de lo parametros nombre + codigo

    const newCallBack = await this.create({
      hash: paymentData.nombre + paymentData.codigo,
    });

    return newCallBack;
  }
}
